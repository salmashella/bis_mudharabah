<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//login
Route::get('/login/','COLogin@index');
Route::post('/onLoginClicked/','COLogin@store');

//logout
Route::get('/logout/', 'COHome@logout');

//Pegawai (Admin)
Route::get('/formpegawai','COTambahPegawai@index');
Route::post('/tambahpegawai','COTambahPegawai@store');
Route::get('/lihatpegawai','COLihatPegawai@index');
Route::get('/updatepegawai/{id}','COEditPegawai@show');
Route::post('/editpegawai/{id}','COEditPegawai@update');

//Home
Route::get('/home','COHome@index');


//Nasabah
Route::get('/formnasabah','COTambahNasabah@index');
Route::post('/tambahnasabah','COTambahNasabah@store');
Route::get('/lihatnasabah', 'COLihatNasabah@index');
Route::get('/nasabah/detail{id}/', 'ControllerDetailNasabah@show');
Route::get('/nasabah/edit/{id}/', 'ControllerEditNasabah@show');

//pembiayaan
Route::get('/formpembiayaan/','COTambahPembiayaan@index');
Route::post('/tambahpembiayaan','COTambahPembiayaan@store');


//rekening
Route::get('/formrekening', 'COTambahRekening@index');
Route::post('/tambahrekening', 'COTambahRekening@store');
Route::get('/lihatrekening', 'COLihatRekening@index');

//Pembiayaan
Route::get('/formpembiayaan', 'COTambahPembiayaan@index');

