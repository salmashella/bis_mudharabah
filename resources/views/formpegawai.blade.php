@extends('header')
@section('content')


 <body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
		<div class="main-content main-content3">
			<div id="page-wrapper">
				<div class="graphs">
					<h3 class="blank1">Form Pegawai</h3>
						<div class="tab-content">
						<div class="tab-pane active" id="horizontal-form">
							<form class="form-horizontal" action="/tambahpegawai" method="post">
								{{csrf_field()}}

                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Nama Pegawai</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control1" id="focusedinput" placeholder="Nama Pegawai" name="Nama_Pegawai" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Alamat</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control1" id="focusedinput" placeholder="Alamat Pegawai" name="Alamat" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="selector1" class="col-sm-2 control-label">Jabatan</label>
                                    <div class="col-sm-8">
                                        <select name="Jabatan" id="selector1" class="form-control1" required="">
                                            <option value="1">Manager</option>
                                            <option value="2">Admin / CSO</option>
                                            <option value="3">Teller</option>
                                        </select></div>
                                </div>
                                <div class="form-group">
                                    <label for="selector1" class="col-sm-2 control-label">Jenis Kelamin</label>
                                    <div class="col-sm-8"><select name="Jenkel" id="selector1" class="form-control1" required="">
                                            <option value="1">Laki-Laki</option>
                                            <option value="2">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">No Telepon</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control1" id="focusedinput" placeholder="No Telepon" name="No_Telp" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="selector1" class="col-sm-2 control-label">Agama</label>
                                    <div class="col-sm-8"><select name="Agama" id="selector1" class="form-control1" required="">
                                            <option value="Islam">Islam</option>
                                            <option value="Konghucu">Konghucu</option>
                                            <option value="Kristen">Kristen</option>
                                            <option value="Protestan">Protestan</option>
                                            <option value="Budha">Budha</option>
                                        </select></div>
                                </div>

                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Tempat Lahir</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control1" id="focusedinput" placeholder="Tempat Lahir" name="Tempat_Lahir" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Tanggal Lahir</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control1 ng-invalid ng-invalid-required" ng-model="model.date" required="" name="Tanggal_Lahir">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="selector1" class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-8"><select name="Status" id="selector1" class="form-control1" required="">
                                            <option value="1">Aktif</option>
                                            <option value="2">Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Username</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control1" id="focusedinput" placeholder="Username" name="Username" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control1" id="focusedinput" placeholder="Password" name="Password" required="">
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <button type="submit" class="btn-success btn">Submit</button>
                                            <a href="home" class="btn-default btn">Cancel</a>
                                            <button type="reset" class="btn-inverse btn">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                        </div>
                </div>
            </div>
    </div>
    </section>
</body>


@endsection