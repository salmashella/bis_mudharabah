@extends('header')
@section('content')

    <body class="sticky-header left-side-collapsed" >
    <!-- main content start-->
    <div class="main-content main-content3">
        <!-- header-starts -->


            <div class="menu-right">
                <hr>
                <div class="user-panel-top">
                    <div class="col-md-9 top-content">
                        <h5>Selamat datang, {{ session('Nama_Pegawai') }}</h5>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                    <!--notification menu end -->
                </div>
                <!-- //header-ends -->
                    <div class="graphs">
                        <div class="col_3">
                            <div class="col-md-3 widget widget1">
                                <div class="r3_counter_box">
                                    <i class="fa fa-mail-forward"></i>
                                    <div class="stats">
                                        <h5>Jumlah Data Nasabah</h5>
                                        <div class="grow" >
                                            <div class="clearfix"> {{ $datanasabah }} </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 widget widget1">
                                <div class="r3_counter_box">
                                    <i class="fa fa-users"></i>
                                    <div class="stats">
                                        <h5>Jumlah Data Pembiayaan</h5>
                                        <div class="grow grow1">
                                            <div class="clearfix"> {{ $datapembiayaan }} </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                        </div>


                    </div>
                    <div class="clearfix"> </div>

                <div class="clearfix"> </div>

            </div>

</body>
@endsection