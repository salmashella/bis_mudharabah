@extends('header')
@section('content')
    <div class="content-top">
        <div class="col-md-12 ">
            <div class="content-top-1">
                <div class="col-md-12">
                    <table class="table">
                        <h3><br><br><br>Daftar Nasabah</h3>
                        <br>
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>ID Nasabah</th>
                            <th>No Rekening</th>
                            <th>Nama Lengkap</th>
                            <th>Tempat, tanggal lahir</th>
                            <th>Alamat</th>
                            <th>No. Identitas</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($result))
                            <?php $a = 0; ?>
                            @foreach($result as $value)
                                <tr>
                                    <th scope="row">{{ ++$a }}</th>
                                    <td>{{ $value->Id_Nasabah }}</td>
                                    <td>{{ $value->No_Rekening }}</td>
                                    <td>{{ $value->Nama }}</td>
                                    <td>{{ $value->Tempat_Lahir }}, {{ $value->Tanggal_Lahir }}</td>
                                    <td>{{ $value->Alamat }}</td>
                                    <td>{{ $value->No_Identitas }}</td>
                                    <td>
                                        <a class="btn-sm btn-danger" href="/nasabah/detail/{{ base64_encode($value->Id_Nasabah) }}">Detail</a><br><br>
                                        @if(session('Jabatan') == 2)
                                            <a class="btn-sm btn-info" href="/nasabah/edit/{{ base64_encode($value->Id_Nasabah) }}">Edit</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>

        <div class="clearfix"> </div>
    </div>
    <br>
@endsection