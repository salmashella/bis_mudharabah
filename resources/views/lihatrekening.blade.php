@extends('header')
@section('content')

    <div class="content-top">
        <div class="col-md-12 ">
            <div class="content-top-1">
                <table class="table">
                    <h3>Daftar Rekening</h3>
                    <br>
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>No Rekening</th>
                        <th>Saldo</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($result))
                        <?php $a = 1 ?>
                        @foreach($result as $value)
                            <tr>
                                <th scope="row">{{ $a }}</th>
                                <td>{{ $value->No_Rekening }}</td>
                                <td>{{ $value->Saldo }}</td>
                                <td>
                                    @if($value->Status == 1)
                                        <font color="blue">Aktif</font>
                                    @else
                                        <font color="red">Tidak Aktif</font>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn-sm btn-danger" href="/detailrekening{{ base64_encode($value->No_Rekening) }}">Detail</a><br><br>

                                    <a class="btn-sm btn-info" href="/editrekening{{ base64_encode($value->No_Rekening) }}">Edit</a>

                                </td>
                            </tr>
                            <?php $a++ ?>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection