@extends('header')
@section('content')
    <script src="/js/customjs.js" type="text/javascript"></script>
    <body class="sticky-header left-side-collapsed">
    <section>
        <div class="main-content main-content3">
            <div id="page-wrapper">
                <div class="graphs">
                    <h3 class="blank1">Form Rekening</h3>
                    <div class="tab-content">
                        <div class="tab-pane active" id="horizontal-form">
                            <form class="form-horizontal" action="/tambahrekening" method="post">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-6" id="datanasabah">
                                        <div class="form-group">
                                            <label form="focusedinput">Data Rekening</label>
                                        </div>

                                        <div class="form-group">
                                            <label for="focusedinput" class="col-sm-2 control-label">No Rekening</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control1" id="focusedinput" placeholder="No Rekening" name="No_Rekening" required="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="focusedinput" class="col-sm-2 control-label">Saldo</label>
                                            <div class="col-sm-8">
                                                <input type="read-only" class="form-control1" id="focusedinput" placeholder="Saldo" name="Saldo" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="focusedinput" class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-8">
                                            <select name="Status" id="selector1" class="form-control1" >
                                                <option value="1">Aktif</option>
                                                <option value="2">Tidak Aktif</option>
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <button type="submit" value="submit" class="btn-success">Submit</button>
                                            <a href="home" class="btn-default btn">Cancel</a>
                                            <button type="reset" class="btn-inverse btn">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>






    </section>
    </body>


@endsection