@extends('header')
@section('content')

    <script src="/js/customjs.js" type="text/javascript"></script>
    <body class="sticky-header left-side-collapsed">
    <section>
        <div class="main-content main-content3">
            <div id="page-wrapper">
                <div class="graphs">
                    <h3 class="blank1">Form Pembiayaan Akad</h3>
                    <div class="tab-content">
                        <div class="tab-pane active" id="horizontal-form">
                            <form class="form-horizontal" action="/tambahpembiayaan" method="post">
                                {{csrf_field()}}


                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">No Rekening</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="No_Rekening" required="true" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Tujuan Akad</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control1" id="focusedinput" placeholder="Tujuan Akad" name="tujuan_akad" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Besar Pembiayaan</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control1" min="0" value="0" onkeypress="hitungAngsuran()" max="10000000" step="100000" id="besarPembiayaan" placeholder="Besar Pembiayaan" name="Besar_Pembiayaan" required="true">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Jangka Waktu</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control1" min="1" max="24" value="1" onkeypress="hitungAngsuran()" id="jangkaWaktu" placeholder="Jangka Waktu Pembiayaan" name="Jangka_Waktu" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Nisbah Nasabah %</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control1" id="focusedinput" placeholder="Nisbah Nasabah" name="Nisbah_Nasabah" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Nisbah BMT %</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control1" id="focusedinput" placeholder="Nisbah BMT" name="Nisbah_BMT" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Angsuran Perbulan</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control1" id="angsuranPerbulan" placeholder="Angsuran Perbulan" name="angsuran_perbulan" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Tanggal Pengajuan</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control1 ng-invalid ng-invalid-required" ng-model="model.date" required="" name="tanggal_pengajuan">
                                    </div>
                                </div>
                                    <div class="form-group">
                                        <label for="focusedinput" class="col-sm-2 control-label">Bentuk jaminan pembiayaan</label>
                                        <select id="Jenis" name="Jenis" class="form-control1">
                                            <option value="BPKB Motor">BPKB Motor</option>
                                            <option value="BPKB Mobil">BPKB Mobil</option>
                                            <option value="Surat Tanah">Sertifikat Rumah</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="focusedinput" class="col-sm-2 control-label">Nomor ID Jaminan</label>
                                        <input type="number" class="form-control1" placeholder="Nomor ID Jaminan" name="Nomor">
                                    </div>

                                    <div class="form-group">
                                        <label for="selector1" class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-8"><select name="Status" id="selector1" class="form-control1" required="">
                                                <option value="Aktif">Aktif</option>
                                                <option value="Tidak_Aktif">Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <button type="submit" class="btn-success btn" value="submit">Submit</button>
                                            <a href="home" class="btn-default btn">Cancel</a>
                                            <button type="reset" class="btn-inverse btn">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </body>

    <script>
        function hitungAngsuran() {
            var besarPembiayaan = document.getElementById('besarPembiayaan').value;
            var jangkaWaktu = document.getElementById('jangkaWaktu').value;
            var angsuranPerbulan = document.getElementById('angsuranPerbulan');

            var total = besarPembiayaan / jangkaWaktu;

            angsuranPerbulan.value = total;
        }
    </script>


@endsection