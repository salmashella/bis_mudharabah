
<!DOCTYPE HTML>
<html>
<head>
    <title>Prima Syariah</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Easy Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <link href="/css/style.css" rel='stylesheet' type='text/css' />
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
    <script src="/js/Chart.js"></script>
    <link href="/css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
    <script src="/js/jquery-1.10.2.min.js"></script>
</head>

<body class="sticky-header left-side-collapsed">
<section>
    <div class="left-side sticky-left-side">

        <div class="logo">
            <h1><a href="/home">Prima Syariah</a></h1>
            <br>
        </div>

        <div class="left-side-inner">

            <ul class="nav nav-pills nav-stacked custom-nav"><br><br><br>
                @if(session('Jabatan') == 1)
                    <li class="menu-list">
                        <br><a href="#"><i class="lnr lnr-pencil"></i>
                            <span>Pembiayaan</span></a>
                        <ul class="sub-menu-list">
                            <li><a href="/Manajer/persetujuan">Verifikasi Pembiayaan</a> </li>
                            <li><a href="/Manajer/listpersetujuan">Daftar Persetujuan</a></li>
                        </ul>
                    </li>
                @endif

                @if(session('Jabatan') == 2)
                    <li class="menu-list"><a href="#"><i class="fa fa-tasks"></i>
                            <span>Pegawai</span></a>
                        <ul class="sub-menu-list">
                            <li><a href="/formpegawai">Tambah Pegawai</a> </li>
                            <li><a href="/lihatpegawai">Daftar Pegawai</a></li>
                        </ul>
                    </li>


                    <li class="menu-list"><a href="#"><i class="lnr lnr-indent-increase"></i>
                            <span>Nasabah</span></a>
                        <ul class="sub-menu-list">
                            <li><a href="/formnasabah">Tambah Nasabah</a> </li>
                            <li><a href="/lihatnasabah">Daftar Nasabah</a></li>
                        </ul>
                    </li>


                    <li class="menu-list"><a href="#"><i class="lnr lnr-book"></i>
                            <span>Rekening</span></a>
                        <ul class="sub-menu-list">
                            <li><a href="/formrekening">Tambah Rekening</a></li>
                            <li><a href="/lihatrekening">Daftar Rekening</a></li>
                        </ul>
                    </li>

                    <li class="menu-list"><a href="#"><i class="lnr lnr-select"></i>
                            <span>Pembiayaan</span></a>
                        <ul class="sub-menu-list">
                            <li><a href="/pembiayaan/addpembiayaan">Tambah Pembiayaan</a> </li>
                            <li><a href="/pembiayaan/listpembiayaan">Daftar Pembiayaan</a></li>
                        </ul>
                    </li>
                @endif


                @if(session('Jabatan') == 3)
                <li class="menu-list"><a href="#"><i class="lnr lnr-envelope"></i> <span>Data Pembayaran</span></a>
                        <ul class="sub-menu-list">
                            <li><a href="pembayaran/addpembayaran">Tambah Pembayaran</a> </li>
                            <li><a href="pembayaran/listpembayaran">Daftar Pembayaran</a></li>
                        </ul>
                </li>
                @endif
            </ul>
            <!--sidebar nav end-->
        </div>
    </div>
    <!-- left side end-->

    <!-- main content start-->
    <div class="main-content main-content2 main-content2copy">
        <!-- header-starts -->
        <div class="header-section">

            <!--toggle button start-->
            <a class="toggle-btn  menu-collapsed"><i class="fa fa-bars"></i></a>
            <!--toggle button end-->

            <div class="profile_details">
                <ul>
                    <li class="dropdown profile_details_drop">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <div class="profile_img">

                                <div class="user-name">
                                    <span>Administrator</span>
                                </div>
                                <i class="lnr lnr-chevron-down"></i>
                                <i class="lnr lnr-chevron-up"></i>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <ul class="dropdown-menu drp-mnu">
                            <li> <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a> </li>
                        </ul>
                    </li>
                    <div class="clearfix"> </div>
                </ul>
            </div>

            <!--footer section start-->

            <!--footer section end-->


        </div>
        @yield('content')
        </div>
</section>

<script src="/js/jquery.nicescroll.js"></script>
<script src="/js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/js/bootstrap.min.js"></script>
</body>
</html>
