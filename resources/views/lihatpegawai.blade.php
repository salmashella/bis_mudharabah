@extends('header')
@section('content')

    <div class="content-top">
        <div class="col-md-12 ">
            <div class="content-top-1">
                <table class="table">
                    <h3>Daftar Admin</h3>
                    <br>
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($result))
                        <?php $a = 1 ?>
                        @foreach($result as $value)
                            <tr>
                                <th scope="row">{{ $a }}</th>
                                <td>{{ $value->Nama_Pegawai }}</td>
                                <td>{{ $value->Username }}</td>
                                <td>
                                    @if($value->Status == 1)
                                        <font color="blue">Aktif</font>
                                    @else
                                        <font color="red">Tidak Aktif</font>
                                    @endif
                                </td>
                                <td>
                                <a class="btn-sm btn-danger" href="/pegawai/detail/{{ base64_encode($value->Id_Pegawai) }}">Detail</a><br><br>

                                    <a class="btn-sm btn-info" href="/updatepegawai/{{ base64_encode($value->Id_Pegawai) }}">Edit</a>

                                </td>
                            </tr>
                            <?php $a++ ?>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection