@extends('header')
@section('content')


    <body class="sticky-header left-side-collapsed">
    <section>
        <div class="main-content main-content3">
            <div id="page-wrapper">
                <div class="graphs">
                    <h3 class="blank1">Form Pegawai</h3>
                    <div class="tab-content">
                        <div class="tab-pane active" id="horizontal-form">
                            <form class="form-horizontal" action="/editpegawai/{id}/" method="post" {{ base64_encode($result->Id_Pegawai) }}>
                                {{csrf_field()}}

                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Nama Pegawai</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control1" id="focusedinput" placeholder="Nama Pegawai" name="Nama_Pegawai" required="" value="{{ $result->Nama_Pegawai }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Alamat</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control1" id="focusedinput" placeholder="Alamat Pegawai" name="Alamat" required="" value="{{ $result->Alamat }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="selector1" class="col-sm-2 control-label">Jabatan</label>
                                    <div class="col-sm-8">
                                        <select name="Jabatan" id="selector1" class="form-control1" required="">
                                            @if($result->Jabatan == 1)
                                            <option value="1">Manager</option>
                                            <option value="2">Admin / CSO</option>
                                            <option value="3">Teller</option>
                                            @elseif($result->Jabatan == 2)
                                                <option value="1">Manager</option>
                                                <option value="2" >Admin / CSO</option>
                                                <option value="3">Teller</option>
                                            @elseif($result->Jabatan == 3)
                                                <option value="1">Manager</option>
                                                <option value="2">Admin / CSO</option>
                                                <option value="3">Teller</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="selector1" class="col-sm-2 control-label">Jenis Kelamin</label>
                                    <div class="col-sm-8">
                                        <select name="Jenkel" id="selector1" class="form-control1" required="">
                                            @if($result->Jenkel == 1)
                                            <option value="1">Laki-Laki</option>
                                            <option value="2">Perempuan</option>
                                            @elseif($result->Jenkel == 2)
                                            <option value="1">Laki-Laki</option>
                                            <option value="2">Perempuan</option>
                                            @endif
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">No Telepon</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control1" id="focusedinput" placeholder="No Telepon" name="No_Telp" required="" value="{{ $result->No_Telp }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="selector1" class="col-sm-2 control-label">Agama</label>
                                    <div class="col-sm-8">
                                        <select name="Agama" id="selector1" class="form-control1" required="">
                                            @if($result->Agama == 'Islam')
                                            <option value="Islam">Islam</option>
                                            <option value="Konghucu">Konghucu</option>
                                            <option value="Kristen">Kristen</option>
                                            <option value="Protestan">Protestan</option>
                                            <option value="Budha">Budha</option>
                                            @elseif($result->Agama == 'Konghucu')
                                            <option value="Islam">Islam</option>
                                            <option value="Konghucu">Konghucu</option>
                                            <option value="Kristen">Kristen</option>
                                            <option value="Protestan">Protestan</option>
                                            <option value="Budha">Budha</option>
                                            @elseif($result->Agama == 'Kristen')
                                            <option value="Islam">Islam</option>
                                            <option value="Konghucu">Konghucu</option>
                                            <option value="Kristen">Kristen</option>
                                            <option value="Protestan">Protestan</option>
                                            <option value="Budha">Budha</option>
                                            @elseif($result->Agama == 'Protestan')
                                            <option value="Islam">Islam</option>
                                            <option value="Konghucu">Konghucu</option>
                                            <option value="Kristen">Kristen</option>
                                            <option value="Protestan">Protestan</option>
                                            <option value="Budha">Budha</option>
                                            @elseif($result->Agama == 'Budha')
                                            <option value="Islam">Islam</option>
                                            <option value="Konghucu">Konghucu</option>
                                            <option value="Kristen">Kristen</option>
                                            <option value="Protestan">Protestan</option>
                                            <option value="Budha">Budha</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Tempat Lahir</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control1" id="focusedinput" placeholder="Tempat Lahir" name="Tempat_Lahir" required="" value="{{ $result->Tempat_Lahir }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Tanggal Lahir</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control1 ng-invalid ng-invalid-required" ng-model="model.date" required="" name="Tanggal_Lahir" value="{{ $result->Tanggal_Lahir }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="selector1" class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-8">
                                        <select name="Status" id="selector1" class="form-control1" required="">
                                            @if($result->Status == 1)
                                            <option value="1">Aktif</option>
                                            <option value="2">Tidak Aktif</option>
                                            @elseif($result->Status == 2)
                                                <option value="2">Tidak Aktif</option>
                                            <option value="1">Aktif</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Username</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control1" id="focusedinput" placeholder="Username" name="Username" required="" value="{{ $result->Username }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="focusedinput" class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control1" id="focusedinput" placeholder="Password" name="Password" required="" value="{{ $result->Password }}">
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <button type="submit" class="btn-success btn">Submit</button>
                                            <a href="home" class="btn-default btn">Cancel</a>
                                            <button type="reset" class="btn-inverse btn">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </body>


@endsection