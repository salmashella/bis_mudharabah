<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPasangan extends Model
{
    //
    protected $table = 'tb_datapasangan';
    protected $fillable = ['Id_Nasabah', 'Nama_Pasangan', 'Pekerjaan_Pasangan', 'Jenkel_Pasangan', 'Keterangan'];

}

