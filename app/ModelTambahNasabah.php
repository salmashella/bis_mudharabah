<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelTambahNasabah extends Model
{
    protected $table = 'tb_nasabah';
    protected $fillable = ['Nama', 'Jenkel', 'Alamat', 'Agama', 'No_Telp', 'Jenis_Identitas', 'No_Identitas', 'Pekerjaan',
        'Tempat_Lahir','Tanggal_Lahir', 'Nama_Ahliwaris', 'Alamat_Ahliwaris', 'Hubungankeluarga_Ahliwaris'];
}
