<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPembiayaan extends Model
{
    protected $table = 'tb_pembiayaan';
    protected $fillable = ['No_Rekening', 'tujuan_akad', 'Besar_Pembiayaan', 'Jangka_Waktu', 'Nisbah_Nasabah',
        'Nisbah_BMT', 'angsuran_perbulan', 'tanggal_pengajuan', 'Jenis_Jaminan', 'No_Jaminan', 'Status'];
}
