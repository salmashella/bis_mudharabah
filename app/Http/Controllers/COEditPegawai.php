<?php

namespace App\Http\Controllers;

use App\ModelTambahPegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class COEditPegawai extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   // public function show()
    public function show($id)
    {
        //
       if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $id = base64_decode($id);

        $datapegawai = DB::Table('tb_Pegawai')
            ->select('tb_Pegawai.*')
            //->where('tb_Pegawai.Id_Pegawai' )
            ->where('tb_Pegawai.Id_Pegawai', '=', $id)
            ->first();

        $title = "Home";
        $content = view('editpegawai');

        View::share('result', $datapegawai);
        return view('header', compact('title', 'content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $ids = base64_decode($id);

        $Nama_Pegawai = $request->input('Nama_Pegawai');
        $Alamat = $request->input('Alamat');
        $Jabatan = $request->input('Jabatan');
        $Jenkel = $request->input('Jenkel');
        $No_Telp = $request->input('No_Telp');
        $Agama = $request->input('Agama');
        $Tempat_Lahir = $request->input('Tempat_Lahir');
        $Tanggal_Lahir = $request->input('Tanggal_Lahir');
        $Status = $request->input('Status');
        $Username = $request->input('Username');
        $Password = $request->input('Password');

        $idadmin = ModelTambahPegawai::where('Id_Pegawai', $ids)->first();

        $updatePegawai['Nama_Pegawai'] = $Nama_Pegawai;
        $updatePegawai['Alamat'] = $Alamat;
        $updatePegawai['Jabatan'] = $Jabatan;
        $updatePegawai['Jenkel'] = $Jenkel;
        $updatePegawai['No_Telp'] = $No_Telp;
        $updatePegawai['Agama'] = $Agama;
        $updatePegawai['Tempat_Lahir'] = $Tempat_Lahir;
        $updatePegawai['Tanggal_Lahir'] = $Tanggal_Lahir;
        $updatePegawai['Status'] = $Status;
        $updatePegawai['Username'] = $Username;
        $updatePegawai['Password'] = $Password;

        if($Password) {
            $Password = Hash::make($request->input('Password'));
            $updatePegawai['Password'] = $Password;
        }



        $dataPegawai = ModelTambahPegawai::where('id_pegawai', $ids)->update($updatePegawai);


            if($dataPegawai) {
                return Redirect::to('/home')->with('message', 'Data admin ' . base64_decode($id) . ' berhasil diubah');
            }
            else {
                return Redirect::to('/home')->with('message', 'Data admin ' . $id . ' gagal diubah');
            }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
