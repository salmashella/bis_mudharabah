<?php

namespace App\Http\Controllers;

use App\ModelRekening;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class COTambahRekening extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $title = "Add Rekening";
        $content = view('formrekening');

        return view('header', compact('title', 'content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $No_Rekening = $request->input('No_Rekening');
        $Saldo = $request->input('Saldo');
        $Status = $request->input('Status');


        $datatambahrekening = ModelRekening::create([
            'No_Rekening' => $No_Rekening,
            'Saldo' => $Saldo,
            'Status' => $Status,

        ]);

        if($datatambahrekening)
        {
            return Redirect::to('lihatrekening');
        }
        else
        {
            echo "gagal";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
