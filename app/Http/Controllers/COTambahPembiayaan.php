<?php

namespace App\Http\Controllers;

use App\ModelPembiayaan;
use Illuminate\Http\Request;

class COTambahPembiayaan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $title = "Add Pembiayaan";
        $content = view('formpembiayaan');

        return view('header', compact('title', 'content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $No_Rekening = $request->input('No_Rekening');
        $tujuan_akad = $request->input('tujuan_akad');
        $Besar_Pembiayaan = $request->input('Besar_Pembiayaan');
        $Jangka_Waktu = $request->input('Jangka_Waktu');
        $Nisbah_Nasabah = $request->input('Nisbah_Nasabah');
        $Nisbah_BMT = $request->input('Nisbah_BMT');
        $angsuran_perbulan = $request->input('angsuran_perbulan');
        $tanggal_pengajuan = $request->input('tanggal_pengajuan');
        $Jenis_Jaminan = $request->input('Jenis_Jaminan');
        $No_Jaminan = $request->input('No_Jaminan');
        $Status = $request->input('Status');

        $datatambahnasabah = ModelPembiayaan::create([

            'No_Rekening' => $No_Rekening,
            'tujuan_akad' => $tujuan_akad,
            'Besar_Pembiayaan' => $Besar_Pembiayaan,
            'Jangka_Waktu' => $Jangka_Waktu,
            'Nisbah_Nasabah' => $Nisbah_Nasabah,
            'Nisbah_BMT' => $Nisbah_BMT,
            'angsuran_perbulan' => $angsuran_perbulan,
            'tanggal_pengajuan' => $tanggal_pengajuan,
            'Jenis_Jaminan' => $Jenis_Jaminan,
            'No_Jaminan' => $No_Jaminan,
            'Status' => $Status,
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
