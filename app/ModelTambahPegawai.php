<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelTambahPegawai extends Model
{
    //
    protected $table = 'tb_pegawai';
    protected $fillable = ['Nama_Pegawai', 'Alamat', 'Jabatan', 'Jenkel', 'No_Telp','Agama',
        'Tempat_Lahir','Tanggal_Lahir','Status', 'Username', 'Password'];
}
